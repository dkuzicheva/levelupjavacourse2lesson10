package org.levelup.thread;

import static sun.misc.PostVMInitHook.run;

public class ThreadApp {

    public static void main(String[] args) throws InterruptedException {

        //extends class Threads
        // implements Runnable interface
        // implements Collable interface


        FirstThread t = new FirstThread();
        t.setName("First thread");
        t.start(); // запускаем поток


        Thread thread = new Thread(new FirstHelloWorldRunnable(),"runnable-thread");
        thread.start();


        t.join(); // main должен ожидать пока поток t не завершит свое выполнение
        thread.join();

        System.out.println("End of " + Thread.currentThread().getName());



    }


    static class FirstThread extends Thread{

        @Override
        public void run() {

            //start point of thread execution

            for (int i =0 ; i<10;i++){
                System.out.println(Thread.currentThread().getName()+ " " + i);
            }

        }
    }



    static class FirstHelloWorldRunnable implements Runnable {


        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + " " + "hello world");
            }
        }
    }









}
