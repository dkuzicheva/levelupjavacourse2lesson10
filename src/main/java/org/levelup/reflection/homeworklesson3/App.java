package org.levelup.reflection.homeworklesson3;

import java.io.IOException;
import java.util.List;

public class App {

    public static  void main (String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        AppService service = new AppService();
        String packageName = "org.levelup.reflection.homeworklesson3.classes";
        String packageNameAdd = "org.levelup.reflection.homeworklesson3.classes.subfolder";
        String packageNameWrong= "homeworklesson3.classes.subfolder";

        List<String> names = service.findClasses(packageName);
        service.createObjForClassesWithoutAnnotation(packageName, names);


        List<String> namesAdd = service.findClasses(packageNameAdd);
        service.createObjForClassesWithoutAnnotation(packageNameAdd, namesAdd);


        service.findClasses(packageNameWrong);
    }
}
