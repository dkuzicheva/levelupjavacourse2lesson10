package org.levelup.reflection.homeworklesson3.classes.subfolder;

import org.levelup.reflection.homeworklesson3.ReflectionClass;

@ReflectionClass
public class Cars {

    private String model;
    private int year;


    public Cars(){
        model = "BMW";
        year = 1988;
    }


    @Override
    public String toString() {
        return "Cars{" +
                "model='" + model + '\'' +
                ", year=" + year +
                '}';
    }
}
