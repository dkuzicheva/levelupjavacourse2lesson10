package org.levelup.reflection.homeworklesson3.classes.subfolder;

public class Teachers {

    private String name;
    private int age;
    private String degree;


    public Teachers(){
        name = "Keith S. Folse";
        age = 31;
        degree = "PhD";
    }


    @Override
    public String toString() {
        return "Teachers{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", degree='" + degree + '\'' +
                '}';
    }
}
