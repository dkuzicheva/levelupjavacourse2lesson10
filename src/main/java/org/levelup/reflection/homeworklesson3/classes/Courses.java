package org.levelup.reflection.homeworklesson3.classes;

import org.levelup.reflection.homeworklesson3.ReflectionClass;

@ReflectionClass
public class Courses {

    private String courseName;
    private int amountOfHours;
    private String teacherName;


    public Courses(){
        this.courseName = "English course. A1.1";
        this.amountOfHours = 68;
        this.teacherName = "Keith S. Folse";
    }


    @Override
    public String toString() {
        return "Courses{" +
                "courseName='" + courseName + '\'' +
                ", amountOfHours=" + amountOfHours +
                ", teacherName='" + teacherName + '\'' +
                '}';
    }


}
