package org.levelup.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Reflection {

    public static  void main (String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException,
            InvocationTargetException, InstantiationException {

    //1 var: есть объект класса
        Phone ph = new Phone();
        Class<?> phoneClassObject = ph.getClass();//? -- wildcard

        System.out.println(phoneClassObject.getName());

    //2 var: нет объекта класса
        Class<?> phClassObject = Phone.class;

        System.out.println(phClassObject.getName());
        System.out.println("Classes are equal:"+(phClassObject==phoneClassObject));

        Field[] declaredField = phClassObject.getDeclaredFields(); // возвращает все поля
                                                                   //getFields() вернет информацию о публичных классах

        Field modelField = phClassObject.getDeclaredField("model");
        System.out.println("Field name:"+modelField.getName());
        System.out.println("Field type:"+modelField.getType().getName());

        Phone samsung = new Phone("Samsung 10",4,2.1);
        modelField.setAccessible(true);
        System.out.println("---------");
        System.out.println("Field model:"+modelField.get(samsung));

        modelField.set(samsung,"Samsung11");
        System.out.println("Field model after changing:"+modelField.get(samsung));


        Constructor constr =  phClassObject.getDeclaredConstructor(double.class);
        constr.setAccessible(true);
        Phone createdPhoneWithPrivateConstructor = (Phone) constr
                .newInstance(4.344d);

        System.out.println(createdPhoneWithPrivateConstructor.getCpu());


    }
}
