package org.levelup.hibernate.config;


import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.levelup.hibernate.domain.*;

import java.util.Properties;


public class HibernateConfigForTestsViaJava {

    private HibernateConfigForTestsViaJava(){

    }

    private static SessionFactory sessionFactory = initializeSessionFactory();


    //создание sessionFactory
    private static SessionFactory initializeSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();

                // Hibernate settings equivalent to hibernate.cfg.xml's properties
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, "org.postgresql.Driver");
                settings.put(Environment.URL, "jdbc:postgresql://localhost:5432/university_integration");
                settings.put(Environment.USER, "postgres");
                settings.put(Environment.PASS, "B3rlin12+");
                settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL10Dialect");
                settings.put(Environment.SHOW_SQL, "true");
                settings.put(Environment.FORMAT_SQL, "true");
                settings.put(Environment.HBM2DDL_AUTO, "create");

                configuration.setProperties(settings);

                configuration.addAnnotatedClass(UniversityEntity.class);
                configuration.addAnnotatedClass(FacultyEntity.class);
                configuration.addAnnotatedClass(SubjectEntity.class);
                configuration.addAnnotatedClass(SubjectInfoEntity.class);
                configuration.addAnnotatedClass(DepartmentEntity.class);
                configuration.addAnnotatedClass(StudentGroupEntity.class);
                configuration.addAnnotatedClass(StudentEntity.class);

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build();

                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }



    //получение объекта SessionFactory
    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

}
