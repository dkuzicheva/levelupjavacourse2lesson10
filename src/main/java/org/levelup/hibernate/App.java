package org.levelup.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.config.HibernateConfiguration;
import org.levelup.hibernate.domain.UniversityEntity;
import org.levelup.hibernate.repository.HibernateUniversityRepositoryImpl;
import org.levelup.hibernate.repository.UniversityRepository;

import java.util.Collection;

public class App {


    public static  void main (String[] args){

        /*
        ClassWork
         */
        SessionFactory sessionFactory = HibernateConfiguration.getFactory();

        UniversityRepository universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);
        UniversityEntity university = universityRepository
                .createUniversity("Лениградский ЭлектроТехнический Интститут 2", "ЛЭТИ 2", 1944, 42343, "Инженерный");

        System.out.println("ID ЛЭТИ 2: " + university.getId());

//        UniversityEntity secondUniversity = universityRepository.findById(109);
//        System.out.println(secondUniversity.getName() + " " + secondUniversity.getShortName() + " " + secondUniversity.getFoundationYear());


        sessionFactory.close();


        /*
        Homework
         */

//        SessionFactory sessionFactory = HibernateConfiguration.getFactory();
//
//        UniversityRepository universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);
//
//        UniversityEntity university = universityRepository.findByName("СПб ГУТ им.проф.Бонч-Бруевича");
//        System.out.println("Result university is:"+ " "+ university.getName() + " " + university.getShortName() + " " + university.getFoundationYear());
//
//        UniversityEntity university1 = universityRepository.findByShortName("СПБГУ");
//        System.out.println("Result university is:"+ " "+ university1.getName() + " " + university1.getShortName() + " " + university1.getFoundationYear());
//
//        UniversityEntity university2 = universityRepository.findByFoundationYear(1866);
//        System.out.println("Result university is:"+ " "+ university2.getName() + " " + university2.getShortName() + " " + university2.getFoundationYear());
//
//        Collection<UniversityEntity> university3 = universityRepository.findAllFoundedUniversitiesBetweenYears(1856, 1894);
//        System.out.println("Result universities are: ");
//        university3.forEach(universityEntity ->
//                System.out.println(universityEntity.getName() + " "+ universityEntity.getShortName() + " "+ universityEntity.getFoundationYear()));
//
//
//
//
//
//        sessionFactory.close();




    }
}
