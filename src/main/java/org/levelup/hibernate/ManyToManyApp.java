package org.levelup.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.config.HibernateConfiguration;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.SubjectEntity;
import org.levelup.hibernate.domain.UniversityEntity;
import org.levelup.hibernate.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ManyToManyApp {


    public static void main(String[] args) {


        SessionFactory factory = HibernateConfiguration.getFactory();

        FacultyRepository repository = new HibernateFacultyRepository(factory);
        SubjectRepository repositorySub = new HibernateSubjectRepositoryImpl(factory);
        FacultySubjectRepository fsRepository = new HibernateFacultySubjectRepositoryImpl(factory);

//  Old methods
//        FacultyEntity faculty1 = repository.createFaculty(102, 6779, "Экономический");
//        repositorySub.createSubject(4, "Экономика", 72);
//        repositorySub.createSubject(5, "Физика", 72);
//        repositorySub.createSubject(6, "Высшая математика", 300);

//   Old methods
//        fsRepository.weaveSubjectAndFaculty(4, 6779);
//        fsRepository.weaveSubjectAndFaculty(5, 6779);
//        fsRepository.weaveSubjectAndFaculty(6, 6779);



////////////////////////////
// Homework. Lesson8
////////////////////////////
        repository.createFaculty(107, 6780, "Ракетостроения");
        FacultyEntity faculty2 = repository.createFaculty(108, 6781, "Самолетостроения");
        FacultyEntity faculty3 = repository.createFaculty(103, 6782, "Кораблестроения");
        FacultyEntity faculty4 = repository.createFaculty(110, 6783, "Машиностроения");

        SubjectEntity subject1 = repositorySub.createSubject(7, "Экономика", 72);
        SubjectEntity subject2 = repositorySub.createSubject(8, "Физика", 72);
        SubjectEntity subject3 = repositorySub.createSubject(9, "Высшая математика", 300);

        List<Integer> facultyIds = Stream.of(faculty2.getFacultyId(),faculty3.getFacultyId(),faculty4.getFacultyId()).collect(Collectors.toList());
        List<Integer> subjectIds = Stream.of(subject1.getId(),subject2.getId(),subject3.getId()).collect(Collectors.toList());



        fsRepository.weaveSubjectAndFaculty(subjectIds, 6780);
        fsRepository.weaveSubjectAndFaculty(9, facultyIds);


        factory.close();


    }
}
