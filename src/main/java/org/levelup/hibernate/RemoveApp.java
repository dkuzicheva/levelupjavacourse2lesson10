package org.levelup.hibernate;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.config.HibernateConfiguration;
import org.levelup.hibernate.repository.*;

public class RemoveApp {

    public static void main(String[] args) {


        SessionFactory factory = HibernateConfiguration.getFactory();

        SubjectRepository repositorySubject = new HibernateSubjectRepositoryImpl(factory);


        repositorySubject.removeSubject(2);



        factory.close();

    }

}
