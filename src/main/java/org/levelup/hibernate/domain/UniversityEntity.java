package org.levelup.hibernate.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "university")
public class UniversityEntity {



@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;

private String name;

@Column(name = "short_name", nullable = false, unique = true)
private String shortName;

@Column(name = "foundation_year", nullable = false)
private Integer foundationYear;

//@JoinColumn(name = "university_id")
@OneToMany(mappedBy = "university")
private List<FacultyEntity> faculties;


    public UniversityEntity(String name, String shortName, Integer foundationYear){
        this.name = name;
        this.shortName = shortName;
        this.foundationYear = foundationYear;
        this.faculties = new ArrayList<>();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UniversityEntity)) return false;
        UniversityEntity that = (UniversityEntity) o;
        return getId().equals(that.getId()) &&
                getName().equals(that.getName()) &&
                getShortName().equals(that.getShortName()) &&
                getFoundationYear().equals(that.getFoundationYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getShortName(), getFoundationYear());
    }
}
