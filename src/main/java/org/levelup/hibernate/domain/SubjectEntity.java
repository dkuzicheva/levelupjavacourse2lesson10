package org.levelup.hibernate.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.Collection;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "subject")
public class SubjectEntity {

    @Id
    private Integer id;
    private String subject;

    @Column(name = "count_of_hours")
    private Integer hours;


    @ManyToMany( mappedBy = "subjects", fetch= FetchType.EAGER)
    private Collection<FacultyEntity> faculties;

    @OneToOne(mappedBy = "subject", cascade = CascadeType.REMOVE)
    private SubjectInfoEntity info;



    public SubjectEntity (Integer id, String name, int hours){
        this.id = id;
        this.subject = name;
        this.hours = hours;
    }




}
