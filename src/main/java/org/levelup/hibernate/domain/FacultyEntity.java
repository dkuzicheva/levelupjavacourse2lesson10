package org.levelup.hibernate.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "faculty")
public class FacultyEntity {



    @Id
    @Column(name = "faculty_id", nullable = false, unique = true)
    private Integer facultyId;

    private String name;


    @ManyToOne()
    @JoinColumn(name = "university_id")
    private UniversityEntity university;



    public FacultyEntity(Integer facultyId, String name){
        this.name = name;
        this.facultyId = facultyId;
    }


@ManyToMany
@JoinTable(
        name = "faculty_subject",
        joinColumns = @JoinColumn (name = "faculty_id"),
        inverseJoinColumns =@JoinColumn (name = "subject_id")
)
private Collection<SubjectEntity> subjects;



@OneToMany(mappedBy = "departmentFacultyId")
private Collection<DepartmentEntity> departments;


}
