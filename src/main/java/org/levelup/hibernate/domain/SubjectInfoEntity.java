package org.levelup.hibernate.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "subject_info")
@NoArgsConstructor
@AllArgsConstructor
public class SubjectInfoEntity {

@Id
@Column(name = "subject_id")
private Integer subjectId;

private String description;

@Column(name = "room_number")
private Integer roomNumber;


@OneToOne(fetch= FetchType.LAZY)
@PrimaryKeyJoinColumn
private SubjectEntity subject;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubjectInfoEntity)) return false;
        SubjectInfoEntity that = (SubjectInfoEntity) o;
        return Objects.equals(getSubjectId(), that.getSubjectId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getRoomNumber(), that.getRoomNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSubjectId(), getDescription(), getRoomNumber());
    }
}
