package org.levelup.hibernate.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.hibernate.domain.*;

import java.util.Date;

@RequiredArgsConstructor
public class HibernateStudentGroupRepository implements StudentGroupRepository{



    private final SessionFactory factory;



    @Override
    public StudentGroupEntity createStudentGroup (String groupKey, Integer groupDepartmentId){
        try (Session s = factory.openSession()) {
            Transaction t = s.beginTransaction();

            StudentGroupEntity studentGroup = new StudentGroupEntity();
            studentGroup.setGroupKey(groupKey);
            studentGroup.setGroupDepartmentId(s.load(DepartmentEntity.class, groupDepartmentId));


            s.persist(studentGroup);

            t.commit();
            return studentGroup;
        }

    }

    @Override
    public StudentGroupEntity getStudentGroupById (String groupKey) {

        try(Session s = factory.openSession()){
            return s.get(StudentGroupEntity.class, groupKey);
        }

    }






}
