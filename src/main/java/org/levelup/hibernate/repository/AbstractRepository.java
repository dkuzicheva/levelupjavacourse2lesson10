package org.levelup.hibernate.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.function.Function;

@RequiredArgsConstructor
public abstract class AbstractRepository {

 protected final SessionFactory factory;

//runWith transaction
protected <T> T runWithTransaction(Function<Session, T> function ) {
    Transaction tx = null;
    try (Session s = factory.openSession()) {
        tx = s.beginTransaction();

        T result = function.apply(s);

        tx.commit();

        return result;
    }catch (Exception exc){
        if (tx != null){
            tx.rollback();
        }
        throw  new RuntimeException(exc);
    }

}

//run (without transaction)

protected <T> T run(SessionExecutor<T> executor) {
    try (Session s = factory.openSession()) {
        return executor.execute(s);
    }


}



}
