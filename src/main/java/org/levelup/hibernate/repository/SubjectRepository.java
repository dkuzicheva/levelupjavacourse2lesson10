package org.levelup.hibernate.repository;

import org.levelup.hibernate.domain.SubjectEntity;

public interface SubjectRepository {


    SubjectEntity createSubject (Integer id, String name, int hours );
    SubjectEntity findById (Integer subjectId );

    void removeSubject(Integer subjectId);

}
