package org.levelup.hibernate.repository;

import org.hibernate.SessionFactory;
import org.levelup.hibernate.domain.SubjectEntity;

//@RequiredArgsConstructor
public class HibernateSubjectRepositoryImpl extends AbstractRepository implements SubjectRepository {

    //private final SessionFactory factory;

    public HibernateSubjectRepositoryImpl(SessionFactory factory){
        super(factory);
    }


    @Override
    public SubjectEntity createSubject(Integer id, String subject, int hours) {
//        try (Session s = factory.openSession()) {
//            Transaction t = s.beginTransaction();
//
//            SubjectEntity sbj = new SubjectEntity(id, subject, hours);
//            s.persist(sbj);
//            t.commit();
//
//            return  sbj;
//        }

        return runWithTransaction(session -> {
            SubjectEntity subj = new SubjectEntity(id, subject, hours);
            session.persist(subj);
            return subj;
        });
    }


    @Override
    public SubjectEntity findById(Integer subjectId) {
        return run(s -> s.createQuery("from SubjectEntity where id=:subjectId", SubjectEntity.class)
                .setParameter("subjectId", subjectId)
                .getSingleResult()
        );
    }

    @Override
    public void removeSubject(Integer subjectId) {
        runWithTransaction(s -> {
                SubjectEntity entity = s.get(SubjectEntity.class, subjectId);
                s.remove(entity);

                return null;
    });


    }


}
