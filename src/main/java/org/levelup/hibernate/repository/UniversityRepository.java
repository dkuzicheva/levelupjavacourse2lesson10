package org.levelup.hibernate.repository;

import org.levelup.hibernate.domain.UniversityEntity;

import java.util.Collection;

public interface UniversityRepository {


UniversityEntity createUniversity(String name, String shortName, Integer foundationyear);
UniversityEntity createUniversity(String name, String shortName, Integer foundationyear, int facultyId, String facultyName);
UniversityEntity findById(Integer universityId);
Collection<UniversityEntity> findAll();


/*
Homework part
 */
UniversityEntity findByName(String universityName);
UniversityEntity findByShortName(String universityShortName);
UniversityEntity findByFoundationYear(Integer universityYear);
Collection<UniversityEntity> findAllFoundedUniversitiesBetweenYears(Integer universityYearFrom, Integer universityYearTo);







}
