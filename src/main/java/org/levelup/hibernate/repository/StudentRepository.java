package org.levelup.hibernate.repository;

import org.levelup.hibernate.domain.DepartmentEntity;
import org.levelup.hibernate.domain.StudentEntity;

import java.sql.Date;
import java.sql.Timestamp;

public interface StudentRepository {


    StudentEntity createStudent (Integer studentCard, String firstName, String lastName, Date birthday , String groupKey);
    StudentEntity getStudentById (Integer studentCard);



}
