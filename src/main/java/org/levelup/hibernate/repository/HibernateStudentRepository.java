package org.levelup.hibernate.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.hibernate.domain.DepartmentEntity;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.StudentEntity;
import org.levelup.hibernate.domain.StudentGroupEntity;

import java.sql.Date;
import java.sql.Timestamp;

@RequiredArgsConstructor
public class HibernateStudentRepository implements StudentRepository{



    private final SessionFactory factory;



    @Override
    public StudentEntity createStudent (Integer studentCard, String firstName, String lastName, Date birthday , String groupKey){
        try (Session s = factory.openSession()) {
            Transaction t = s.beginTransaction();

            StudentEntity student = new StudentEntity();
            student.setStudentCard(studentCard);
            student.setFirstName(firstName);
            student.setLastName(lastName);
            student.setBirthday(birthday);

            student.setStudentGroupKey(s.load(StudentGroupEntity.class, groupKey));

            s.persist(student);

            t.commit();
            return student;
        }

    }

    @Override
    public StudentEntity getStudentById (Integer studentCard) {

        try(Session s = factory.openSession()){
            return s.get(StudentEntity.class, studentCard);
        }

    }






}
