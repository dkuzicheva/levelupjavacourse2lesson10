package org.levelup.hibernate.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.hibernate.domain.DepartmentEntity;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.UniversityEntity;

import java.util.ArrayList;

@RequiredArgsConstructor
public class HibernateDepartmentRepository implements DepartmentRepository{


    private final SessionFactory factory;



    @Override
    public DepartmentEntity createDepartment(Integer facultyId, Integer departmentId, String name) {
        try (Session s = factory.openSession()) {
            Transaction t = s.beginTransaction();

            DepartmentEntity department = new DepartmentEntity();
            department.setId(departmentId);
            department.setName(name);
            department.setDepartmentFacultyId(s.load(FacultyEntity.class, facultyId));

            s.persist(department);

            t.commit();
            return department;
        }

    }

    @Override
    public DepartmentEntity getDepartmentById(Integer departmentId) {

        try(Session s = factory.openSession()){
            return s.get(DepartmentEntity.class, departmentId);
        }

    }


}
