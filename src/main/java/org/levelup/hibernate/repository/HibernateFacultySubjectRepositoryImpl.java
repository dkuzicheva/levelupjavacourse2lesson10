package org.levelup.hibernate.repository;


import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.SubjectEntity;
import org.levelup.hibernate.domain.UniversityEntity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public class HibernateFacultySubjectRepositoryImpl implements FacultySubjectRepository {

    private final SessionFactory factory;



//////////////////////////////////////////////////
//Old method
//////////////////////////////////////////////////
//    @Override
//    public void weaveSubjectAndFaculty( Integer subjectId, Integer facultyId) {
//
//        try (Session s = factory.openSession()) {
//
//            Transaction t = s.beginTransaction();
//
//             SubjectEntity subj = s.get(SubjectEntity.class, subjectId);
//             FacultyEntity faculty = s.get(FacultyEntity.class, facultyId);
//
//             subj.getFaculties().add(faculty);
//             faculty.getSubjects().add(subj);
//
//             t.commit();
//
////             s.merge(faculty);
////             s.merge(subj);
//
//        }
//
//    }




////////////////////////////
// Homework. Lesson8
////////////////////////////
    @Override
    public void weaveSubjectAndFaculty(List<Integer> subjectIds, Integer facultyId) {

        try (Session s = factory.openSession()) {

            Transaction t = s.beginTransaction();

            FacultyEntity faculty = s.get(FacultyEntity.class, facultyId);
            subjectIds.forEach((subjectId) -> {
                SubjectEntity subj = s.get(SubjectEntity.class, subjectId);
                subj.getFaculties().add(faculty);
                faculty.getSubjects().add(subj);
            });

            t.commit();

        }
    }

    @Override
    public void weaveSubjectAndFaculty(Integer subjectId, List<Integer> facultyIds) {
        try (Session s = factory.openSession()) {

            Transaction t = s.beginTransaction();


            SubjectEntity subj = s.get(SubjectEntity.class, subjectId);
            facultyIds.forEach((facultyId) -> {
                FacultyEntity faculty = s.get(FacultyEntity.class, facultyId);
                faculty.getSubjects().add(subj);
                subj.getFaculties().add(faculty);

            });

            t.commit();
        }
    }


}
