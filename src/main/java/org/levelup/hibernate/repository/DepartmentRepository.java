package org.levelup.hibernate.repository;



import org.levelup.hibernate.domain.DepartmentEntity;


public interface DepartmentRepository {

 DepartmentEntity createDepartment (Integer facultyId, Integer departmentId, String name);
 DepartmentEntity getDepartmentById (Integer departmentId);





}
