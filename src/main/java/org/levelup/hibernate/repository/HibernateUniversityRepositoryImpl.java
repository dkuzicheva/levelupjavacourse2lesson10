package org.levelup.hibernate.repository;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.UniversityEntity;

import java.util.Collection;

//@RequiredArgsConstructor
public class HibernateUniversityRepositoryImpl extends AbstractRepository implements UniversityRepository {


    //private final SessionFactory factory;

    public HibernateUniversityRepositoryImpl(SessionFactory factory){
        super(factory);
    }


    @Override
    public UniversityEntity createUniversity(String name, String shortName, Integer foundationyear) {


        try (Session s = factory.openSession()) {
            Transaction transaction = s.beginTransaction();

            UniversityEntity university = new UniversityEntity(name, shortName, foundationyear);
            s.persist(university); // делаем инсерт в таблицу university
            transaction.commit(); // фиксируем все изменения

            return university;
        }

    }

    @Override
    public UniversityEntity findById(Integer universityId) {

//        try (Session s = factory.openSession()) {
//            return s.createQuery("from UniversityEntity where id = :id", UniversityEntity.class)
//                    .setParameter("id", universityId)
//                    .getSingleResult();
//
//        }


        return run(s ->
                        s.createQuery("from UniversityEntity where id = :id", UniversityEntity.class)
                    .setParameter("id", universityId)
                    .getSingleResult());
    }

    @Override
    public Collection<UniversityEntity> findAll() {


//        SessionExecutor<Collection<UniversityEntity>> allUniversitiesExecutor = new SessionExecutor<Collection<UniversityEntity>>() {
//            @Override
//            public Collection<UniversityEntity> execute(Session s) {
//                return  s.createQuery("from UniversityEntiry", UniversityEntity.class)
//                        .getResultList();
//            }
//        };
//
//
//        return run(allUniversitiesExecutor);

         return run(s -> s.createQuery("from UniversityEntiry", UniversityEntity.class).getResultList());

    }

    @Override
    public UniversityEntity createUniversity(String name, String shortName, Integer foundationyear, int facultyId, String facultyName) {
        try (Session s = factory.openSession()) {
            Transaction transaction = s.beginTransaction();

            UniversityEntity university = new UniversityEntity(name, shortName, foundationyear);

            FacultyEntity faculty = new FacultyEntity();
            faculty.setFacultyId(facultyId);
            faculty.setName(facultyName);
            faculty.setUniversity(university);


            //university.getFaculties().add(faculty);
            s.persist(university); // делаем инсерт в таблицу university
            s.persist(faculty); // делаем инсерт в таблицу university

            transaction.commit(); // фиксируем все изменения

            return university;
        }
    }



/* ---------------------------------------------------------------------------------------------------------------------
Homework part. Lesson 6
 ---------------------------------------------------------------------------------------------------------------------*/


    @Override
    public UniversityEntity findByName(String universityName) {

        if(universityName == null){
            throw new IllegalArgumentException("UniversityName should be not null");
        }
        try (Session s = factory.openSession()) {
            return s.createQuery("from UniversityEntity where name = :name", UniversityEntity.class)
                    .setParameter("name", universityName)
                    .getSingleResult();

        }
    }

    @Override
    public UniversityEntity findByShortName(String universityShortName) {
        if(universityShortName == null || universityShortName.length() > 20){
            throw new IllegalArgumentException("Wrong UniversityShortName. It should be not null with lenght leass 20 symbols");
        }
        try (Session s = factory.openSession()) {
            return s.createQuery("from UniversityEntity where shortName = :shortName", UniversityEntity.class)
                    .setParameter("shortName", universityShortName)
                    .getSingleResult();
        }
    }

    @Override
    public UniversityEntity findByFoundationYear (Integer universityYear){
        if(universityYear<0){
            throw new IllegalArgumentException("Foundation year must be more than 0");
        }
            try (Session s = factory.openSession()) {
                return s.createQuery("from UniversityEntity where foundationYear = :foundationYear", UniversityEntity.class)
                        .setParameter("foundationYear", universityYear)
                        .getSingleResult();


            }
        }

    @Override
    public Collection<UniversityEntity> findAllFoundedUniversitiesBetweenYears (Integer universityYearFrom, Integer universityYearTo){
        if(universityYearFrom<0 || universityYearTo<0){
            throw new IllegalArgumentException("Please check the parameters. Both foundation years must be more than 0");
        }
        try (Session s = factory.openSession()) {
            return s.createQuery("from UniversityEntity where foundationYear > :foundationYear1 and foundationYear <= :foundationYear2", UniversityEntity.class)
                    .setParameter("foundationYear1", universityYearFrom)
                    .setParameter("foundationYear2", universityYearTo)
                    .getResultList();
        }
        }



}
