package org.levelup.universities.jdbc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class App {


    public static  void main (String[] args){

        UniversitiesJdbcStorage jdbcStorage = new UniversitiesJdbcStorage(new JdbcService());

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){

//            System.out.println("Enter name:");
//            String name = reader.readLine();
//
//            System.out.println("Enter short name:");
//            String shortName = reader.readLine();
//
//            System.out.println("Enter foundation year:");
//            int foundationYear = Integer.parseInt(reader.readLine());
//
//            jdbcStorage.createUniversity(name, shortName, foundationYear);
//            jdbcStorage.displayUniversities();

            jdbcStorage.findBySql("select * from university where foundation_year > ?", 1900);
            System.out.println("-----");
            jdbcStorage.findBySql("select * from university where short_name like ?", "СПБ%");

        }catch(IOException exp){
            throw new RuntimeException(exp);
        }






    }
}
