package org.levelup.universities.jdbc.homework01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AppHomework {


    public static  void main (String[] args){

        FacultiesJdbcStorageHomework jdbcStorage = new FacultiesJdbcStorageHomework();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){


            System.out.println("Enter faculty name:");
            String name = reader.readLine();

            System.out.println("Enter ID of university:");
            int university_id = Integer.parseInt(reader.readLine());

            jdbcStorage.createFaculty(name, university_id);
            jdbcStorage.displayFaculties();
            System.out.println("-----");
            jdbcStorage.findBySql("select * from faculty where name like ?", "%ный%");
            System.out.println("-----");


        }catch(IOException exp){
            throw new RuntimeException(exp);
        }






    }
}
