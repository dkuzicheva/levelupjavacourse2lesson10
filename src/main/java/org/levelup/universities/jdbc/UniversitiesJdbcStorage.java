package org.levelup.universities.jdbc;

import java.sql.*;

public class UniversitiesJdbcStorage {


    public JdbcService jdbcService;

    public UniversitiesJdbcStorage(JdbcService jdbcService){
        this.jdbcService = jdbcService;
    }


    public void createUniversity(String name, String shortName, int year){
        if(year<0){
            throw new IllegalArgumentException("Foundation year must be more than 0");
        }

        //Создаем новую запись в таблице universities

        try (Connection connection = jdbcService.openConnection()){

            //Statement , PreparedStatement, CallableStatement

            PreparedStatement statement = connection.prepareStatement("insert into university (name, short_name, foundation_year) values (?, ?, ?)");

            statement.setString(1, name);
            statement.setString(2,shortName);
            statement.setInt(3,year);

            int rowsAffected = statement.executeUpdate();
            System.out.println("Rows affected:" + rowsAffected);

        }catch(SQLException exp){
            System.out.println("Couldnot open connection"+exp.getMessage());
            throw new RuntimeException();
        }


    }

    public void displayUniversities() {
        try (Connection connection = jdbcService.openConnection()) {

            //Statement , PreparedStatement, CallableStatement

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("select * from university");
            while (result.next()) {
                //result -- указатель на текущую строку в цикле
                int id = result.getInt(1);
                String name = result.getString(2);
                String shortName = result.getString("short_name");
                int year = result.getInt(4);

                System.out.println(id + " " + name + " " + shortName + " " + year);

            }


        } catch (SQLException exp) {
            System.out.println("Couldnot open connection" + exp.getMessage());
            throw new RuntimeException();
        }
    }



    public void findBySql(String sql,Object...args){

            try (Connection connection = jdbcService.openConnection()) {

                //Statement , PreparedStatement, CallableStatement

                PreparedStatement statement = connection.prepareStatement(sql);

                int parameterIndex = 1;

                for (Object argument: args) {
                    statement.setObject(parameterIndex++, argument);
                }
                ResultSet result = statement.executeQuery();
                while (result.next()) {
                    //result -- указатель на текущую строку в цикле
                    int id = result.getInt(1);
                    String name = result.getString(2);
                    String shortName = result.getString("short_name");
                    int year = result.getInt(4);

                    System.out.println(id + " " + name + " " + shortName + " " + year);

                }

            } catch (SQLException exp) {
                System.out.println("Couldnot open connection" + exp.getMessage());
                throw new RuntimeException();
            }
        }



    }





