package org.levelup.universities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.universities.jdbc.JdbcService;
import org.levelup.universities.jdbc.UniversitiesJdbcStorage;
import org.mockito.*;
import org.mockito.verification.VerificationMode;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.Mockito.when;

public class UniversitiesJdbcStorageTest {

    private UniversitiesJdbcStorage jdbcStorage;

    @Mock
    private JdbcService jdbcService;

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement statement;


    @BeforeEach
    public void setupJdbcStorage() throws SQLException {
        //1 variant
        //jdbcService = Mockito.mock(JdbcService.class);

        //2 variant --- with annotation
        MockitoAnnotations.openMocks(this);
        jdbcStorage = new UniversitiesJdbcStorage(jdbcService);

        when(jdbcService.openConnection())
                .thenReturn(connection);


        when(connection.prepareStatement(ArgumentMatchers.anyString()))
                .thenReturn(statement);

    }



    @Test
    public void testCreateUniversity_whenDataIsValid_thenCreateUniversity() throws SQLException {

            String name = "name";
            String shortName = "short name";
            int  year = 1984;

            //when
            jdbcStorage.createUniversity(name, shortName, year);

            //then
            Mockito.verify(statement, Mockito.times(0)).executeUpdate();

    }


    @Test
    public void testCreateUniversity_whenFoundationYearIsNegative_thenThrowException() {

        String name = "name";
        String shortName = "short name";
        int  year = -1984;

//----------------------------------------------------------------------------------------------------
// test will fail because method returns   IllegalArgumentException and not IllegalAccessException
//----------------------------------------------------------------------------------------------------
//        Assertions.assertThrows(IllegalAccessException.class,
//                () -> jdbcStorage.createUniversity(name, shortName, year));
//----------------------------------------------------------------------------------------------------

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> jdbcStorage.createUniversity(name, shortName, year));

        Assertions.assertTrue(exception.getMessage().equals("Foundation year must be more than 0"));


    }


    @Test
    public void testCreateUniversity_whenOpenConnectionCalled_thenThrowException() throws SQLException {

        String name = "name";
        String shortName = "short name";
        int  year = 1984;

        Mockito.when(jdbcService.openConnection()).thenThrow(new RuntimeException("Can't open connection"));
        //Mockito.doThrow(new RuntimeException("Can't open connection")).when(jdbcService).openConnection();

        try {
            jdbcStorage.createUniversity(name, shortName, year);
        }catch (Throwable exception){
            Assertions.assertEquals(exception.getMessage(), "Can't open connection");
        }

    }














}
