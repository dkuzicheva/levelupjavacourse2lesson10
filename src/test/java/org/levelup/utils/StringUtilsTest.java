package org.levelup.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringUtilsTest {

    @Test
    public void testIsBlank_whenValueIsNull_thenReturnTrue(){
        //given
        //when
        boolean result = StringUtils.isBlank(null);
        //then
        Assertions.assertTrue(result);

    }


    @Test
    public void testIsBlank_whenValueIsEmptyString_thenReturnTrue(){
        //given
        String str = " ";
        //when
        boolean result = StringUtils.isBlank(str);
        //then
        Assertions.assertTrue(result);


    }


    @Test
    public void testIsBlank_whenValueIsWhiteSpaces_thenReturnFalse(){
        //given
        String str = "   4345   ";
        //when
        boolean result = StringUtils.isBlank(str);
        //then
        Assertions.assertFalse(result);


    }

}
