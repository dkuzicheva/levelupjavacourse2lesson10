package org.levelup.integrationtests;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.hibernate.config.HibernateConfigForTestsViaJava;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.SubjectEntity;
import org.levelup.hibernate.domain.UniversityEntity;
import org.levelup.hibernate.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FacultySubjectIntegrationTest {

    private static SessionFactory sessionFactory;

    private static UniversityRepository universityRepository;
    private static FacultyRepository facultyRepository;
    private static SubjectRepository subjectRepository;
    private static FacultySubjectRepository facultySubjectRepository;

    static String universityName, universityShortName;
    static String facultyName, secondFacultyName;
    static String subject, secondSubject ;
    static Integer universityFoundationYear;
    static Integer facultyId, secondFacultyId;
    static Integer subjectId, secondSubjectId;
    static Integer countOfHours;



    @BeforeAll
    public static void setupSessionFactoryAndTestData(){
        sessionFactory = HibernateConfigForTestsViaJava.getSessionFactory();

        universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);
        facultyRepository = new HibernateFacultyRepository(sessionFactory);
        subjectRepository = new HibernateSubjectRepositoryImpl(sessionFactory);
        facultySubjectRepository = new HibernateFacultySubjectRepositoryImpl(sessionFactory);


        universityName = "Санкт-Петербургский государственный архитектурно-строительный университет";
        universityShortName = "СПБ ГАСУ";
        universityFoundationYear = 1967;

        facultyId = 1;
        facultyName = "Строительство";

        secondFacultyId = 2;
        secondFacultyName = "Архитектура";

        subjectId = 1;
        countOfHours = 60;
        subject = "Test Subject 1";


        secondSubjectId = 2;
        secondSubject = "Test Subject 2";

    }



    @BeforeEach
    public void initializeDatabase() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String deleteStudentGroupQuery = "delete from StudentGroupEntity";
            String deleteStudentQuery = "delete from StudentEntity";
            String deleteDepartmentQuery = "delete from DepartmentEntity";
            String deleteFacultyQuery = "delete from FacultyEntity";
            String deleteUniversityQuery = "delete from UniversityEntity";
            String deleteQuery = "delete from SubjectEntity";


            session.createQuery(deleteStudentQuery).executeUpdate();
            session.createQuery(deleteStudentGroupQuery).executeUpdate();
            session.createQuery(deleteDepartmentQuery).executeUpdate();
            session.createQuery(deleteFacultyQuery).executeUpdate();
            session.createQuery(deleteUniversityQuery).executeUpdate();
            session.createQuery(deleteQuery).executeUpdate();

            transaction.commit();
        }
    }

    @AfterAll
    static void closeSessionFactory() {
        sessionFactory.close();
    }

    @Test
    public void testWeaveSubjectAndFaculty_listOfFacultyIds() {
        SubjectEntity subjectEntity = subjectRepository.createSubject(subjectId, subject, countOfHours);
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        FacultyEntity secondFaculty = facultyRepository.createFaculty(university.getId(), secondFacultyId, facultyName);

        //create list of facultyIds
        List<Integer> listFaculty = new ArrayList<>();
        listFaculty.add(faculty.getFacultyId());
        listFaculty.add(secondFaculty.getFacultyId());


        facultySubjectRepository.weaveSubjectAndFaculty(subjectEntity.getId(), listFaculty);

        SubjectEntity foundSubjectEntity = subjectRepository.findById(subjectEntity.getId());
        assertEquals(foundSubjectEntity.getFaculties().size(), 2);

        List<Integer> result = foundSubjectEntity.getFaculties()
                               .stream().map(FacultyEntity::getFacultyId).collect(Collectors.toList());
        assertEquals(listFaculty, result);
    }



    @Test
    public void testWeaveSubjectAndFaculty_listOfSubjectIds() {
        SubjectEntity subjectEntity = subjectRepository.createSubject(subjectId, subject, countOfHours);
        SubjectEntity secondSubjectEntity = subjectRepository.createSubject(secondSubjectId, secondSubject, countOfHours);
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);

        //create list of subjectIds
        List<Integer> listSubjects = new ArrayList<>();
        listSubjects.add(subjectEntity.getId());
        listSubjects.add(secondSubjectEntity.getId());

        facultySubjectRepository.weaveSubjectAndFaculty(listSubjects, faculty.getFacultyId());

        SubjectEntity foundSubjectEntity = subjectRepository.findById(subjectEntity.getId());
        assertEquals(foundSubjectEntity.getFaculties().size(), 1);
        assertEquals(((List<FacultyEntity>) foundSubjectEntity.getFaculties()).get(0).getFacultyId(), facultyId);


        SubjectEntity foundSecondSubjectEntity = subjectRepository.findById(secondSubjectEntity.getId());
        assertEquals(foundSecondSubjectEntity.getFaculties().size(), 1);
        assertEquals(((List<FacultyEntity>) foundSecondSubjectEntity.getFaculties()).get(0).getFacultyId(), facultyId);
    }
}
