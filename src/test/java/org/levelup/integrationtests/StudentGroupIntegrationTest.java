package org.levelup.integrationtests;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.hibernate.config.HibernateConfigForTestsViaJava;
import org.levelup.hibernate.domain.*;
import org.levelup.hibernate.repository.*;

import javax.persistence.PersistenceException;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class StudentGroupIntegrationTest {



    private static SessionFactory sessionFactory;

    private static UniversityRepository universityRepository;
    private static FacultyRepository facultyRepository;
    private static DepartmentRepository departmentRepository;
    private static StudentGroupRepository studentGroupRepository;

    static String universityName, universityShortName;
    static String facultyName;
    static String departmentName;
    static String groupKey;
    static Integer universityFoundationYear;
    static Integer facultyId;
    static Integer departmentId;



    @BeforeAll
    public static void setupSessionFactoryAndTestData(){
        sessionFactory = HibernateConfigForTestsViaJava.getSessionFactory();

        universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);
        facultyRepository = new HibernateFacultyRepository(sessionFactory);
        departmentRepository = new HibernateDepartmentRepository(sessionFactory);
        studentGroupRepository = new HibernateStudentGroupRepository(sessionFactory);


        universityName = "Санкт-Петербургский государственный архитектурно-строительный университет";
        universityShortName = "СПБ ГАСУ";
        universityFoundationYear = 1967;

        facultyId = 1;
        facultyName = "Строительство";

        departmentId = 100;
        departmentName = "Департамент";

        groupKey = "Группа 1";


    }



    @BeforeEach
    public void initializeDatabase() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String deleteStudentGroupQuery = "delete from StudentGroupEntity";
            String deleteStudentQuery = "delete from StudentEntity";
            String deleteDepartmentQuery = "delete from DepartmentEntity";
            String deleteFacultyQuery = "delete from FacultyEntity";
            String deleteUniversityQuery = "delete from UniversityEntity";

            session.createQuery(deleteStudentQuery).executeUpdate();
            session.createQuery(deleteStudentGroupQuery).executeUpdate();
            session.createQuery(deleteDepartmentQuery).executeUpdate();
            session.createQuery(deleteFacultyQuery).executeUpdate();
            session.createQuery(deleteUniversityQuery).executeUpdate();

            transaction.commit();
        }
    }

    @AfterAll
    static void closeSessionFactory() {
        sessionFactory.close();
    }

    @Test
    public void testCreateStudentGroup_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);
        //create group
        StudentGroupEntity groupEntity = studentGroupRepository.createStudentGroup(groupKey, department.getId());

        assertEquals(groupEntity.getGroupKey(), groupKey);
        assertEquals(groupEntity.getGroupDepartmentId().getId(), department.getId());
    }


    @Test
    public void testCreateStudentGroup_withSameGroupId_Exception() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);
        StudentGroupEntity groupEntity = studentGroupRepository.createStudentGroup(groupKey, department.getId());

        //create student with the same card
        try {
            studentGroupRepository.createStudentGroup(groupKey, department.getId()+1);
        }catch (Exception e){
            assertEquals(PersistenceException.class, e.getClass());
            assertEquals("org.hibernate.exception.ConstraintViolationException: could not execute statement", e.getLocalizedMessage());
        }
    }


    @Test
    public void testGetStudentGroupById_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);
        //create group
        StudentGroupEntity groupEntity = studentGroupRepository.createStudentGroup(groupKey, department.getId());


        //get group
        StudentGroupEntity foundGroup = studentGroupRepository.getStudentGroupById(groupKey);


        assertEquals(foundGroup.getGroupKey(), groupKey);
        assertEquals(foundGroup.getGroupDepartmentId().getId(), department.getId());
    }


    @Test
    public void testGetStudentGroupById_NoResult_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);

        StudentGroupEntity foundGroup = studentGroupRepository.getStudentGroupById(groupKey);

        assertNull(foundGroup);

    }


}
