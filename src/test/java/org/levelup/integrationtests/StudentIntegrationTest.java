package org.levelup.integrationtests;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;
import org.levelup.hibernate.config.HibernateConfigForTestsViaJava;
import org.levelup.hibernate.domain.*;
import org.levelup.hibernate.repository.*;

import javax.persistence.PersistenceException;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

public class StudentIntegrationTest {

    private static SessionFactory sessionFactory;

    private static UniversityRepository universityRepository;
    private static FacultyRepository facultyRepository;
    private static DepartmentRepository departmentRepository;
    private static StudentGroupRepository studentGroupRepository;
    private static StudentRepository studentRepository;

    static String universityName, universityShortName;
    static String facultyName;
    static String departmentName;
    static String groupKey;
    static Integer universityFoundationYear;
    static Integer facultyId;
    static Integer departmentId;
    static Integer studentCard;
    static String lastName, firstName;
    static Date birthday;


    @BeforeAll
    public static void setupSessionFactoryAndTestData(){
        sessionFactory = HibernateConfigForTestsViaJava.getSessionFactory();

        universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);
        facultyRepository = new HibernateFacultyRepository(sessionFactory);
        departmentRepository = new HibernateDepartmentRepository(sessionFactory);
        studentGroupRepository = new HibernateStudentGroupRepository(sessionFactory);
        studentRepository = new HibernateStudentRepository(sessionFactory);


        universityName = "Санкт-Петербургский государственный архитектурно-строительный университет";
        universityShortName = "СПБ ГАСУ";
        universityFoundationYear = 1967;

        facultyId = 1;
        facultyName = "Строительство";

        departmentId = 100;
        departmentName = "Департамент";

        groupKey = "Группа 1";

        studentCard = 1;

        lastName = "Петров";
        firstName = "Петр";
        birthday = Date.valueOf("1989-06-10");

    }



    @BeforeEach
    public void initializeDatabase() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String deleteStudentGroupQuery = "delete from StudentGroupEntity";
            String deleteStudentQuery = "delete from StudentEntity";
            String deleteDepartmentQuery = "delete from DepartmentEntity";
            String deleteFacultyQuery = "delete from FacultyEntity";
            String deleteUniversityQuery = "delete from UniversityEntity";

            session.createQuery(deleteStudentQuery).executeUpdate();
            session.createQuery(deleteStudentGroupQuery).executeUpdate();
            session.createQuery(deleteDepartmentQuery).executeUpdate();
            session.createQuery(deleteFacultyQuery).executeUpdate();
            session.createQuery(deleteUniversityQuery).executeUpdate();

            transaction.commit();
        }
    }

    @AfterAll
    static void closeSessionFactory() {
        sessionFactory.close();
    }

    @Test
    public void testCreateStudent_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);
        StudentGroupEntity groupEntity = studentGroupRepository.createStudentGroup(groupKey, department.getId());

        //create student
        StudentEntity studentEntity = studentRepository.createStudent(studentCard, firstName, lastName, birthday, groupEntity.getGroupKey());

        assertEquals(studentEntity.getStudentCard(), studentCard);
        assertEquals(studentEntity.getLastName(), lastName);
        assertEquals(studentEntity.getFirstName(), firstName);
        assertEquals(studentEntity.getBirthday(), birthday);
        assertEquals(studentEntity.getStudentGroupKey().getGroupKey(), groupEntity.getGroupKey());
    }



    @Test
    public void testCreateStudent_withSameStudentCard_Exception() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);
        StudentGroupEntity groupEntity = studentGroupRepository.createStudentGroup(groupKey, department.getId());

        //create student with the same card
        try {
            studentRepository.createStudent(studentCard,"Иван", "Петров", birthday, groupEntity.getGroupKey());
        }catch (Exception e){
            assertEquals(PersistenceException.class, e.getClass());
            assertEquals("org.hibernate.exception.ConstraintViolationException: could not execute statement", e.getLocalizedMessage());
        }
    }


    @Test
    public void testGetStudentById_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);
        StudentGroupEntity groupEntity = studentGroupRepository.createStudentGroup(groupKey, department.getId());

        //create student
        StudentEntity studentEntity = studentRepository.createStudent(studentCard, firstName, lastName, birthday, groupEntity.getGroupKey());


        StudentEntity foundStudent = studentRepository.getStudentById(studentCard);

        assertEquals(studentEntity.getStudentCard(), studentCard);
        assertEquals(studentEntity.getLastName(), lastName);
        assertEquals(studentEntity.getFirstName(), firstName);
        assertEquals(studentEntity.getBirthday(), birthday);
        assertEquals(studentEntity.getStudentGroupKey().getGroupKey(), groupEntity.getGroupKey());
    }


    @Test
    public void testGetStudentById_NoResult_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);
        DepartmentEntity department = departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);
        StudentGroupEntity groupEntity = studentGroupRepository.createStudentGroup(groupKey, department.getId());


        StudentEntity foundStudent = studentRepository.getStudentById(studentCard);

        assertNull(foundStudent);

    }





}
















