package org.levelup.integrationtests;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;
import org.levelup.hibernate.config.HibernateConfigForTestsViaJava;
import org.levelup.hibernate.domain.SubjectEntity;
import org.levelup.hibernate.repository.HibernateSubjectRepositoryImpl;
import org.levelup.hibernate.repository.SubjectRepository;

import javax.persistence.NoResultException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class SubjectIntegrationTest {
    private static SessionFactory sessionFactory;
    private static SubjectRepository subjectRepository;

    static Integer id;
    static int countOfHours;
    static String subject;



    @BeforeAll
    public static void setupSessionFactoryAndTestData(){
        sessionFactory = HibernateConfigForTestsViaJava.getSessionFactory();
        subjectRepository = new HibernateSubjectRepositoryImpl(sessionFactory);

        id = 1;
        countOfHours = 60;
        subject = "Test Subject";

    }


    @BeforeEach
    public void cleanDataFromUniversity() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String deleteQuery = "delete from SubjectEntity";
            session.createQuery(deleteQuery).executeUpdate();
            transaction.commit();
        }
    }

    @AfterAll
    static void closeSessionFactory() {
        sessionFactory.close();
    }



    @Test
    public void testCreateSubject_Success() {
        SubjectEntity subjectEntity = subjectRepository.createSubject(id, subject, countOfHours);

        assertEquals(subjectEntity.getId(), id);
        assertEquals(subjectEntity.getSubject(), subject);
        assertEquals(subjectEntity.getHours(), countOfHours);
    }

    @Test
    public void testFindById_Success() {
        SubjectEntity subjectEntity = subjectRepository.createSubject(id, subject, countOfHours);
        SubjectEntity foundSubjectEntity = subjectRepository.findById(subjectEntity.getId());


        assertEquals(subjectEntity.getId(), foundSubjectEntity.getId());
        assertEquals(subjectEntity.getSubject(), foundSubjectEntity.getSubject());
        assertEquals(subjectEntity.getHours(), foundSubjectEntity.getHours());
    }

    @Test
    public void testRemoveSubject_Success() {
        SubjectEntity subjectEntity = subjectRepository.createSubject(id, subject, countOfHours);

        subjectRepository.removeSubject(subjectEntity.getId());

        assertThrows(NoResultException.class,  () -> subjectRepository.findById(subjectEntity.getId()));
    }













}
