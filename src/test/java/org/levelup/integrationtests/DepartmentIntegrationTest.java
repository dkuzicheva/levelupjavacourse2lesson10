package org.levelup.integrationtests;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.hibernate.config.HibernateConfigForTestsViaJava;
import org.levelup.hibernate.domain.DepartmentEntity;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.UniversityEntity;
import org.levelup.hibernate.repository.*;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DepartmentIntegrationTest {

    private static SessionFactory sessionFactory;

    private static UniversityRepository universityRepository;
    private static FacultyRepository facultyRepository;
    private static DepartmentRepository departmentRepository;

    static String universityName, universityShortName;
    static String facultyName;
    static String departmentName;
    static Integer universityFoundationYear;
    static Integer facultyId;
    static Integer departmentId;




    @BeforeAll
    public static void setupSessionFactoryAndTestData(){
        sessionFactory = HibernateConfigForTestsViaJava.getSessionFactory();

        universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);
        facultyRepository = new HibernateFacultyRepository(sessionFactory);
        departmentRepository= new HibernateDepartmentRepository(sessionFactory);


        universityName = "Санкт-Петербургский государственный архитектурно-строительный университет";
        universityShortName = "СПБ ГАСУ";
        universityFoundationYear = 1967;

        facultyId = 1;
        facultyName = "Строительство";

        departmentId = 1;
        departmentName = "Строительство жилых домов";



    }



    @BeforeEach
    public void initializeDatabase() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String deleteStudentGroupQuery = "delete from StudentGroupEntity";
            String deleteStudentQuery = "delete from StudentEntity";
            String deleteDepartmentQuery = "delete from DepartmentEntity";
            String deleteFacultyQuery = "delete from FacultyEntity";
            String deleteUniversityQuery = "delete from UniversityEntity";

            session.createQuery(deleteStudentQuery).executeUpdate();
            session.createQuery(deleteStudentGroupQuery).executeUpdate();
            session.createQuery(deleteDepartmentQuery).executeUpdate();
            session.createQuery(deleteFacultyQuery).executeUpdate();
            session.createQuery(deleteUniversityQuery).executeUpdate();

            transaction.commit();
        }
    }

    @AfterAll
    static void closeSessionFactory() {
        sessionFactory.close();
    }


    @Test
    public void testCreateDepartment_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);

        //insert department
        DepartmentEntity department= departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);


        assertEquals(department.getName(), departmentName);
        assertEquals(department.getDepartmentFacultyId().getFacultyId(), faculty.getFacultyId());

    }


    @Test
    public void testCreateDepartment_withSameDepartmentId_Exception() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);

        //insert department
        DepartmentEntity department= departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);


        //create department with the same card
        try {
            departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, "Проектирование");
        }catch (Exception e){
            assertEquals(PersistenceException.class, e.getClass());
            assertEquals("org.hibernate.exception.ConstraintViolationException: could not execute statement", e.getLocalizedMessage());
        }
    }


    @Test
    public void testGetDepartmentById_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);

        //insert department
        DepartmentEntity department= departmentRepository.createDepartment(faculty.getFacultyId(), departmentId, departmentName);

        //search department
        DepartmentEntity foundDepartment = departmentRepository.getDepartmentById(department.getId());

        assertEquals(foundDepartment.getId(), department.getId());
        assertEquals(foundDepartment.getName(), department.getName());

    }


    @Test
    public void testGetDepartmentById_NoResult_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);

        //search department
        DepartmentEntity foundDepartment = departmentRepository.getDepartmentById(departmentId);

        assertNull(foundDepartment);
    }


}
