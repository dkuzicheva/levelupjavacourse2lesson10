package org.levelup.integrationtests;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.hibernate.config.HibernateConfigForTestsViaJava;
import org.levelup.hibernate.domain.DepartmentEntity;
import org.levelup.hibernate.domain.FacultyEntity;
import org.levelup.hibernate.domain.StudentGroupEntity;
import org.levelup.hibernate.domain.UniversityEntity;
import org.levelup.hibernate.repository.*;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class FacultyIntegrationTest {

    private static SessionFactory sessionFactory;

    private static UniversityRepository universityRepository;
    private static FacultyRepository facultyRepository;

    static String universityName, universityShortName;
    static String facultyName;
    static Integer universityFoundationYear;
    static Integer facultyId;




    @BeforeAll
    public static void setupSessionFactoryAndTestData(){
        sessionFactory = HibernateConfigForTestsViaJava.getSessionFactory();

        universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);
        facultyRepository = new HibernateFacultyRepository(sessionFactory);


        universityName = "Санкт-Петербургский государственный архитектурно-строительный университет";
        universityShortName = "СПБ ГАСУ";
        universityFoundationYear = 1967;

        facultyId = 1;
        facultyName = "Строительство";



    }



    @BeforeEach
    public void initializeDatabase() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String deleteStudentGroupQuery = "delete from StudentGroupEntity";
            String deleteStudentQuery = "delete from StudentEntity";
            String deleteDepartmentQuery = "delete from DepartmentEntity";
            String deleteFacultyQuery = "delete from FacultyEntity";
            String deleteUniversityQuery = "delete from UniversityEntity";

            session.createQuery(deleteStudentQuery).executeUpdate();
            session.createQuery(deleteStudentGroupQuery).executeUpdate();
            session.createQuery(deleteDepartmentQuery).executeUpdate();
            session.createQuery(deleteFacultyQuery).executeUpdate();
            session.createQuery(deleteUniversityQuery).executeUpdate();

            transaction.commit();
        }
    }

    @AfterAll
    static void closeSessionFactory() {
        sessionFactory.close();
    }

    @Test
    public void testCreateFaculty_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        //create faculty
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);


        assertEquals(faculty.getFacultyId(), facultyId);
        assertEquals(faculty.getName(), facultyName);
        assertEquals(faculty.getUniversity().getId(), university.getId());
    }

    @Test
    public void testCreateFaculty_withSameFacultyId_Exception() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        //create faculty
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);


        //create faculty with the same id
        try {
            facultyRepository.createFaculty(university.getId(), facultyId, "Архитектура");
        }catch (Exception e){
            assertEquals(PersistenceException.class, e.getClass());
            assertEquals("org.hibernate.exception.ConstraintViolationException: could not execute statement", e.getLocalizedMessage());
        }
    }


    @Test
    public void testGetFacultyById_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        //create faculty
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);

        //search faculty
        FacultyEntity foundFaculty = facultyRepository.getById(faculty.getFacultyId());

        assertEquals(faculty.getFacultyId(), foundFaculty.getFacultyId());
        assertEquals(faculty.getName(), foundFaculty.getName());
        assertEquals(faculty.getUniversity().getId(), foundFaculty.getUniversity().getId());
    }

    @Test
    public void testGetFacultyById_NoResult_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);

        //search faculty
        FacultyEntity foundFaculty = facultyRepository.getById(facultyId);

        assertNull(foundFaculty);

    }


    @Test
    public void testLoadFacultyById_Success() {
        UniversityEntity university = universityRepository.createUniversity(universityName, universityShortName, universityFoundationYear);
        //create faculty
        FacultyEntity faculty = facultyRepository.createFaculty(university.getId(), facultyId, facultyName);

        //load faculty
        FacultyEntity foundFaculty = facultyRepository.loadById(faculty.getFacultyId());

        assertEquals(faculty.getFacultyId(), foundFaculty.getFacultyId());
    }



}
