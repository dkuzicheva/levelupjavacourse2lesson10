package org.levelup.integrationtests;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;
import org.levelup.hibernate.domain.UniversityEntity;
import org.levelup.hibernate.repository.UniversityRepository;
import org.levelup.hibernate.repository.HibernateUniversityRepositoryImpl;
import org.levelup.hibernate.config.HibernateConfigForTestsViaJava;


import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class UniversityIntegrationTest {

    private static SessionFactory sessionFactory;
    private static UniversityRepository universityRepository;

    static Integer firstFoundationYear, secondFoundationYear, thirdFoundationYear;
    //Integer firstId, secondId;
    static String firstName, secondName, thirdName;
    static String firstShortName, secondShortName, thirdShortName;




    public UniversityEntity insertUniversityTestData(String name, String shortName, Integer foundationYear){
        return universityRepository.createUniversity(name, shortName, foundationYear);
    }

    public UniversityEntity findUniversityDataByName(UniversityEntity university){
        return universityRepository.findByName(university.getName());
    }

    public UniversityEntity findUniversityDataByShortName(UniversityEntity university){
        return universityRepository.findByShortName(university.getShortName());
    }

    public UniversityEntity findUniversityDataByFoundationYear(UniversityEntity university){
        return universityRepository.findByFoundationYear(university.getFoundationYear());
    }

    public Collection<UniversityEntity> findUniversityDataBetweenFoundationYears(Integer firstYear, Integer secondYear){
        return universityRepository.findAllFoundedUniversitiesBetweenYears(firstYear, secondYear);
    }


    @BeforeAll
    public static void setupSessionFactoryAndTestData(){
        sessionFactory = HibernateConfigForTestsViaJava.getSessionFactory();
        universityRepository = new HibernateUniversityRepositoryImpl(sessionFactory);

        firstName = "Университет телекоммуникаций им. проф.Бонч-Бруевича";
        firstShortName = "СПб ГУТ";
        firstFoundationYear = 1948;

        secondName = "Санкт-Петербургский Институт точной механики и оптики";
        secondShortName = "ИТМО";
        secondFoundationYear = 1999;

        thirdName = "Санкт-Петербургский Финансово-экономический институт";
        thirdShortName = "ФИНЭК";
        thirdFoundationYear = 2001;

    }


    @BeforeEach
    public void cleanDataFromUniversity() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            String deleteQuery = "delete from UniversityEntity";
            session.createQuery(deleteQuery).executeUpdate();
            transaction.commit();
        }
    }

    @AfterAll
    static void closeSessionFactory() {
        sessionFactory.close();
    }




    @Test
    public void testCreateUniversity_Success() {
        UniversityEntity university = insertUniversityTestData(firstName, firstShortName, firstFoundationYear);

        assertEquals(university.getName(), firstName);
        assertEquals(university.getShortName(), firstShortName);
        assertEquals(university.getFoundationYear(), firstFoundationYear);
    }

    @Test
    public void testCreateUniversity_withSameShortName_Exception() {
        //insert test data
        insertUniversityTestData(firstName, firstShortName, firstFoundationYear);

        //insert data with same shortName
        try {
            insertUniversityTestData(secondName, firstShortName, firstFoundationYear);
        }catch (Exception e){
            assertEquals(PersistenceException.class, e.getClass());
            assertEquals("org.hibernate.exception.ConstraintViolationException: could not execute statement", e.getLocalizedMessage());
        }

    }

    @Test
    public void testFindByName_Success() {
        //insert test data
        UniversityEntity university = insertUniversityTestData(firstName, firstShortName, firstFoundationYear);
        //try to find this data
        UniversityEntity universityFound = findUniversityDataByName(university);


        assertEquals( university.getId(), universityFound.getId());
        assertEquals( university.getName(), universityFound.getName());
        assertEquals( university.getShortName(), universityFound.getShortName());
        assertEquals( university.getFoundationYear(), universityFound.getFoundationYear());
    }

    @Test
    public void testFindByShortName_Success() {
        //insert test data
        UniversityEntity university = insertUniversityTestData(firstName, firstShortName, firstFoundationYear);
        //try to find this data
        UniversityEntity universityFound = findUniversityDataByShortName(university);


        assertEquals( university.getId(), universityFound.getId());
        assertEquals( university.getName(), universityFound.getName());
        assertEquals( university.getShortName(), universityFound.getShortName());
        assertEquals( university.getFoundationYear(), universityFound.getFoundationYear());
    }

    @Test
    public void testFindByFoundationYear_Success() {
        //insert test data
        UniversityEntity university = insertUniversityTestData(firstName, firstShortName, firstFoundationYear);
        //try to find this data
        UniversityEntity universityFound = findUniversityDataByFoundationYear(university);


        assertEquals( university.getId(), universityFound.getId());
        assertEquals( university.getName(), universityFound.getName());
        assertEquals( university.getShortName(), universityFound.getShortName());
        assertEquals( university.getFoundationYear(), universityFound.getFoundationYear());
    }

    @Test
    public void testfindAllFoundedUniversitiesBetweenYears_Success() {
        UniversityEntity firstUniversity = insertUniversityTestData(firstName, firstShortName, firstFoundationYear);
        UniversityEntity secondUniversity = insertUniversityTestData(secondName, secondShortName, secondFoundationYear);
        UniversityEntity thirdUniversity = insertUniversityTestData(thirdName, thirdShortName, thirdFoundationYear);

        Collection<UniversityEntity> universities = new ArrayList<>();
        universities.add(firstUniversity);
        universities.add(secondUniversity);
        universities.add(thirdUniversity);

        Integer startYear = firstUniversity.getFoundationYear();
        Integer endYear = thirdUniversity.getFoundationYear();

        Collection<UniversityEntity> universityList = findUniversityDataBetweenFoundationYears(startYear, endYear);
        assertEquals(universityList.size(), 2);
        assertTrue(universityList.contains(secondUniversity));
        assertTrue(universityList.contains(thirdUniversity));

    }






}
