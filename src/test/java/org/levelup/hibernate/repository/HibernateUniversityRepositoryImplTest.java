package org.levelup.hibernate.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.jupiter.api.*;
import org.levelup.hibernate.domain.UniversityEntity;
import org.mockito.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class HibernateUniversityRepositoryImplTest {



/* ---------------------------------------------------------------------------------------------------------------------
Homework part. Lesson 6
 ---------------------------------------------------------------------------------------------------------------------*/


    private HibernateUniversityRepositoryImpl hibernateImpl;


    @Mock
    private SessionFactory factory;
    @Mock
    private  Session session;

    @Mock
    private  Query query;


    @BeforeEach
    public void setupSession(){
        MockitoAnnotations.openMocks(this);
       // MockedStatic<HibernateConfiguration> hibernateConfigurationMock = mockStatic(HibernateConfiguration.class);
       // hibernateConfigurationMock.when(HibernateConfiguration::getFactory)
       //         .thenReturn(factory);


        hibernateImpl = new HibernateUniversityRepositoryImpl(factory);

        when(factory.openSession()).thenReturn(session);



    }




    @Test
    public void testFindUniversityByName_whenNoConnection_thenException(){

        String name = "name";

        doThrow(new RuntimeException("Can't open connection")).when(factory).openSession();
        //then
        Assertions.assertThrows(RuntimeException.class,
                () -> hibernateImpl.findByName(name));

    }

    @Test
    public void testFindUniversityById_whenNoConnection_thenException(){

        String shortName = "name";

        doThrow(new RuntimeException("Can't open connection")).when(factory).openSession();
        //then
        Assertions.assertThrows(RuntimeException.class,
                () -> hibernateImpl.findByShortName(shortName));

    }


    @Test
    public void testFindUniversityByFoundationYear_whenNoConnection_thenException(){

        Integer universityYear = 1234;

        doThrow(new RuntimeException("Can't open connection")).when(factory).openSession();
        //then
        Assertions.assertThrows(RuntimeException.class,
                () -> hibernateImpl.findByFoundationYear(universityYear));

    }


    @Test
    public void testFindAllFoundedUniversitiesBetweenYears_whenNoConnection_thenException(){

        Integer universityYearFrom = 1234;
        Integer universityYearTo = 12345;

        doThrow(new RuntimeException("Can't open connection")).when(factory).openSession();
        //then
        Assertions.assertThrows(RuntimeException.class,
                () -> hibernateImpl.findAllFoundedUniversitiesBetweenYears(universityYearFrom, universityYearTo));

    }


    @Test
    public void testFindUniversityByName_whenWrongParameter_thenException(){

        String wrongName = null;

        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> hibernateImpl.findByName(wrongName));

        Assertions.assertTrue(exception.getMessage().equals("UniversityName should be not null"));
    }


    @Test
    public void testFindUniversityByShortName_whenWrongParameter_thenException(){

        String wrongShortNameFirst = null;
        String wrongShortNameSecond = "Name that is more than 20 symbols";

        Exception exception1 = Assertions.assertThrows(IllegalArgumentException.class,
                () -> hibernateImpl.findByShortName(wrongShortNameFirst));


        Exception exception2 = Assertions.assertThrows(IllegalArgumentException.class,
                () -> hibernateImpl.findByShortName(wrongShortNameSecond));

        Assertions.assertTrue(exception1.getMessage().equals("Wrong UniversityShortName. It should be not null with lenght leass 20 symbols"));
        Assertions.assertTrue(exception2.getMessage().equals("Wrong UniversityShortName. It should be not null with lenght leass 20 symbols"));
    }


    @Test
    public void testFindUniversityByFoundationYear_whenWrongParameter_thenException(){

        Integer universityYear = -1985;

        Exception exception1 = Assertions.assertThrows(IllegalArgumentException.class,
                () -> hibernateImpl.findByFoundationYear(universityYear));

        Assertions.assertTrue(exception1.getMessage().equals("Foundation year must be more than 0"));

    }


    @Test
    public void testFindAllFoundedUniversitiesBetweenYears_whenWrongParameter_thenException(){

        Integer universityYearFrom = -1985;
        Integer universityYearTo = 1985;

        Exception exception1 = Assertions.assertThrows(IllegalArgumentException.class,
                () -> hibernateImpl.findAllFoundedUniversitiesBetweenYears(universityYearFrom, universityYearTo));

        Assertions.assertTrue(exception1.getMessage().equals("Please check the parameters. Both foundation years must be more than 0"));

    }


    @Test
    public void testFindUniversityByName_whenDataIsValid_thenReturnEntity(){

        UniversityEntity entity = new UniversityEntity("First University", "Uni", 1967);


        given(session.createQuery("from UniversityEntity where name = :name", UniversityEntity.class)).willReturn(query);
        given(query.setParameter("name", entity.getName())).willReturn(query);
        given(query.getSingleResult()).willReturn(entity);


        UniversityEntity result = hibernateImpl.findByName(entity.getName());

        Mockito.verify(query, Mockito.times(1));
        Assertions.assertEquals(result, entity);


    }


    @Test
    public void testFindUniversityByShortName_whenDataIsValid_thenReturnEntity(){

        UniversityEntity entity = new UniversityEntity("First University", "Uni", 1968);


        given(session.createQuery("from UniversityEntity where shortName = :shortName", UniversityEntity.class)).willReturn(query);
        given(query.setParameter("shortName", entity.getShortName())).willReturn(query);
        given(query.getSingleResult()).willReturn(entity);


        UniversityEntity result = hibernateImpl.findByShortName(entity.getShortName());

        Mockito.verify(query, Mockito.times(1));
        Assertions.assertEquals(result, entity);


    }


    @Test
    public void testFindUniversityByFoundationYear_whenDataIsValid_thenReturnEntity(){

        UniversityEntity entity = new UniversityEntity("First University", "Uni", 2007);


        given(session.createQuery("from UniversityEntity where foundationYear = :foundationYear", UniversityEntity.class)).willReturn(query);
        given(query.setParameter("foundationYear", entity.getFoundationYear())).willReturn(query);
        given(query.getSingleResult()).willReturn(entity);


        UniversityEntity result = hibernateImpl.findByFoundationYear(entity.getFoundationYear());

        Mockito.verify(query, Mockito.times(1));
        Assertions.assertEquals(result, entity);


    }


    @Test
    public void testFindAllFoundedUniversitiesBetweenYears_whenDataIsValid_thenReturnEntity(){

        UniversityEntity entityFirst = new UniversityEntity("First University 1", "Uni1", 2007);
        UniversityEntity entitySecond = new UniversityEntity("First University 2", "Uni2", 1567);
        Collection<UniversityEntity> collection = new ArrayList<>();
        collection.add(entityFirst);
        collection.add(entitySecond);

        given(session.createQuery("from UniversityEntity where foundationYear > :foundationYear1 and foundationYear <= :foundationYear2", UniversityEntity.class)).willReturn(query);
        given(query.setParameter("foundationYear1", entityFirst.getFoundationYear())).willReturn(query);
        given(query.setParameter("foundationYear2", entitySecond.getFoundationYear())).willReturn(query);
        given(query.getResultList()).willReturn((List) collection);


        Collection<UniversityEntity> result = hibernateImpl
                .findAllFoundedUniversitiesBetweenYears(entityFirst.getFoundationYear(), entitySecond.getFoundationYear());

        Mockito.verify(query, Mockito.times(1));
        Assertions.assertTrue(result.containsAll(collection));


    }


}
